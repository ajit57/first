<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap.min.css">
    <title>
        GET
    </title>
</head>
<body>
<div class="container">
    <h2>Get Form</h2>

    <form action="captureGet.php" method="get">
        <div class="form-group">
            <label for="">Your Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group">
            <label for="">Your Email</label>
            <input type="email" class="form-control" name="email">
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>

</body>

</html>
